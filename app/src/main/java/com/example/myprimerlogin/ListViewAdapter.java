package com.example.myprimerlogin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class ListViewAdapter extends ArrayAdapter<String> {

    private Context context;
    private String[] animals;

    public ListViewAdapter(@NonNull Context context, String[] animals) {
        super(context, 0);
        this.context = context;
        this.animals = animals;
    }


    @NonNull
    @Override
    public View getView(int position, @NonNull View convertView, @NonNull ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = View.inflate(context, R.layout.row_list_animal, parent);
        ImageView imageView = view.findViewById(R.id.animal_image);
        switch (animals[position]) {
            case "Perro":
                imageView.setImageResource(R.drawable.perro);
                break;
            case "Halcon":
                imageView.setImageResource(R.drawable.halcon);
                break;
            case "Leon":
                imageView.setImageResource(R.drawable.leon);
                break;
            case "Delfin":
                imageView.setImageResource(R.drawable.delfin);
                break;
        }

        return view;
    }

    @Override
    public int getCount() {
        return animals.length;
    }
}
