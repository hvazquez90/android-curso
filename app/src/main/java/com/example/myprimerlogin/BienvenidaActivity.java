package com.example.myprimerlogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class BienvenidaActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Spinner spinner;
    private ImageView imageView;
    private String[] options = {"Perro", "Leon", "Delfin", "Halcon"};
    private Button btnFondo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvenida);

        spinner = findViewById(R.id.spinner);
        imageView = findViewById(R.id.image_animal);
        btnFondo = findViewById(R.id.btn_fondo);
        ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, options);
        spinner.setAdapter(aa);

        spinner.setOnItemSelectedListener(this);

        btnFondo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BienvenidaActivity.this,FondosActivity.class));
            }
        });

        TextView tvBienvenida = findViewById(R.id.text_bienvenida);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String nombre = getString(R.string.welcome_string) + " " + bundle.getString("nombre", "");
            tvBienvenida.setText(nombre);
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this, options[position], Toast.LENGTH_SHORT).show();
        switch (options[position]){
            case "Perro":
                imageView.setImageResource(R.drawable.perro);
                break;
            case "Leon":
                imageView.setImageResource(R.drawable.leon);
                break;
            case "Delfin":
                imageView.setImageResource(R.drawable.delfin);
                break;
            case "Halcon":
                imageView.setImageResource(R.drawable.halcon);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
