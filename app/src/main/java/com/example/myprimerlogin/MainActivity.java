package com.example.myprimerlogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    int n,c;
    private EditText etNameId;
    private EditText etPassword;
    private Button btnIngresar;
    private String[][] usuarios = {{"Luis", "123"},{"Mario","456"},{"Jorge","789"}};
    private Boolean existe = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNameId = findViewById(R.id.input_id_name);
        etPassword = findViewById(R.id.input_password);
        btnIngresar = findViewById(R.id.btn_ingresar);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = "";
                String psw =  etPassword.getText().toString();
                String usu = etNameId.getText().toString();

                for (n = 0; n < usuarios.length; n++) {
                    if (usu.equals(usuarios[n][0]) && psw.equals(usuarios[n][1])) {
                        existe = true;
                        nombre = usuarios[n][0];
                    }
                }

                    if (existe) {
                        existe =false;
                        Intent intent = new Intent(MainActivity.this, BienvenidaActivity.class);
                        intent.putExtra("nombre", nombre);
                        startActivity(intent);
                        //startActivity(new Intent(MainActivity.this,BienvenidaActivity.class));
                    } else {
                        existe = false;
                        Toast.makeText(MainActivity.this, "El usuario no existe", Toast.LENGTH_SHORT).show();
                    }

            }
        });

    }
}
