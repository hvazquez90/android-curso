package com.example.myprimerlogin;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class FondosActivity extends AppCompatActivity implements View.OnClickListener, MiprimerFragment.MyprimerFragmentListener, SegundoFragment.SegundoFragmentListener {

    private String[] usuarios = {"Leon","Perro","Halcon","Delfin"};
    private Button btnAgregar;
    private ImageView imgPrincipal;
    private Button btnFragment;
    private Button btnFragment2;
    private MiprimerFragment fragment;
    private SegundoFragment segundoFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fondos);

        btnAgregar = findViewById(R.id.agregar_fondo);
        imgPrincipal = findViewById(R.id.image_principal);
        btnFragment = findViewById(R.id.btn_fragment);
        btnFragment2 = findViewById(R.id.btn_fragment2);

        btnAgregar.setOnClickListener(this);
        btnFragment.setOnClickListener(this);
        btnFragment2.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.agregar_fondo:
             /*   Bitmap bm = BitmapFactory.decodeResource(getResources(), image);
                WallpaperManager wm = WallpaperManager.getInstance(getApplicationContext());

              *//*  try {
                    wm.setBitmap(bm);
                    Toast.makeText(this, "Se agrego tu imagen", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Hubo un error", Toast.LENGTH_SHORT).show();
                }*/


                break;
            case R.id.btn_fragment:
                fragment = new MiprimerFragment();
                fragment.show(getSupportFragmentManager(), "miprimerFragment");
                fragment.variables("uno", "dos");
                break;
            case R.id.btn_fragment2:
                if (segundoFragment == null) {
                    btnFragment2.setVisibility(View.GONE);
                    btnAgregar.setVisibility(View.GONE);
                    btnFragment.setVisibility(View.GONE);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    segundoFragment = new SegundoFragment();
                    transaction.add(R.id.conteiner, segundoFragment);
                    transaction.commit();
                }
                break;
        }

    }

    @Override
    public void onClickClose(String numero, Integer letra) {
        fragment.dismiss();

    }

    @Override
    public void onClickCloseFragment2() {
        getSupportFragmentManager().beginTransaction().remove(segundoFragment).commit();
        btnFragment2.setVisibility(View.VISIBLE);
        btnAgregar.setVisibility(View.VISIBLE);
        btnFragment.setVisibility(View.VISIBLE);
        segundoFragment = null;
        //transaction.hide(segundoFragment);
    }
}
